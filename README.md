# COMP2215 Source Code

This repository contains submodules to all of the other projects in this group.

[See the group here.](https://gitlab.com/jsb1g18-comp2215-source-code "Click this link to see the group on Gitlab")

## How to clone me

```bash
git clone --recurse-submodules https://gitlab.com/jsb1g18-comp2215-source-code/00_all.git
```

## How to update me

```bash
git pull
git submodule update --recursive --remote
```

